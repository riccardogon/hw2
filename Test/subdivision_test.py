import sys
import heat2D.mainprog as h
import matplotlib.pyplot as plt
import numpy as np


sys.path.append(r'C:\Users\richi\Documents\Git\HW_2\heat2D')

n_x = 20
l = 1
x = np.linspace(0, 1, n_x+1)
x_h = np.ones_like(x)


unif = np.linspace(0, l, n_x+1)
cos = (1 - np.cos(np.linspace(0, np.pi/2, n_x+1))) * l
tan = (np.tan(np.linspace(0, np.pi/4, n_x + 1))) * l
plot = [unif, tan]
pl_name = ['uniform', 'tangent', 'log']
pl_col = ['b', 'r', 'g']
pl_mk = ['.', '.']

for i, dist in enumerate(plot):
    plt.plot(x, dist, color=pl_col[i], label=pl_name[i])
    plt.plot(x_h+0.05+0.03*i, dist, pl_mk[i], color=pl_col[i])

# plt.grid()
plt.legend()

