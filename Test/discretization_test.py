import sys
import heat2D.mainprog as h
import matplotlib.pyplot as plt


sys.path.append(r'C:\Users\richi\Documents\Git\HW_2\heat2D')

input_file = r'Input_test.dat'


# n_x = [i for i in range(10, 130, 10)]
n_x = [i for i in range(10, 110, 5)]
n_y = [1]


conv = True

un = []
cos = []
tan = []

for j in n_y:
    un_i = []
    cos_i = []
    tan_i = []
    for i in n_x:
        un_i.append(h.compute(inp_path=input_file, convection=conv, discr='unif', debug=True, n_x=i, n_y=j))
        # cos_i.append(h.compute(inp_path=input_file, convection=conv, discr='cos', debug=True, n_x=i, n_y=j))
        tan_i.append(h.compute(inp_path=input_file, convection=conv, discr='tan', debug=True, n_x=i, n_y=j))
    un.append(un_i)
    cos.append(cos_i)
    tan.append(tan_i)
    plt.plot(n_x, un_i)
    plt.plot(n_x, tan_i)


# plt.plot(un, 'b')
# plt.plot(cos, 'k')
# plt.plot(tan, 'r')

# plt.legend()
# plt.grid()
plt.show()

