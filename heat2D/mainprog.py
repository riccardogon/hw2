"""
This script computes the heat flux per unit of width of a fin. The temperature distribution is considered bidimensional,
so T = T(x, y). Other assumptions are: Steady statem constant thermal conductivity along the fin, no thermal generation
within the fin, the fin surface is considered gray diffusive and the fluid is not participating nor emitting.

For an example of the input format see Input_example.dat in the Examples folder.




           0                 Surface1
         0 +-------------------------------------------+-------> x
           |                                           |
           |                                           |
 Neumann   |                                           | Surface2
 Surface 0 |                                           |
           |                                           |
           |                                           |
           +-------------------------------------------+
           |                  Surface3
           |
           v y

Author: Riccardo Gon
Contact: riccardo.gon@studenti.units.it
"""


import numpy as np
from pandas import read_csv
from scipy.sparse.linalg import spsolve
from scipy.sparse import spdiags


def c2k(celsius):
    kelvin = celsius + 273.15
    return kelvin


def read_input(inp_pat):
    raw_data = read_csv(filepath_or_buffer=inp_pat, sep='\s+', index_col=0, usecols=[0, 1])
    return raw_data


def start(out_len):
    print(out_len * '-')
    print('|' + 'Starting heat2D'.center(out_len - 2, ' ') + '|')
    print(out_len * '-')


def end(out_len, i, q, nx, ny):
    print('|' + (out_len-2)*' ' + '|')
    print('|' + 'Number of cells: {}'.format(nx*ny).center(out_len-2, ' ') + '|')
    print('|' + 'Finished in {:} iterations'.format(i).center(out_len-2, ' ') + '|')
    print('|' + 'Computed heat flux: {:.2f}'.format(q).center(out_len-2, ' ') + '|')
    print('|' + (out_len-2)*' ' + '|')
    print(out_len * '-')


def output(q, T, file_out, x_pts, y_pts):
    with open(file_out, 'w') as file:
        file.write('Results'.center(40, '-') + '\n')
        file.write('Heat flux'.ljust(20, ' ') + str(q) + '\n\n')
        file.write('X points:\n {}\n\n'.format(x_pts.reshape(len(x_pts), 1)))
        file.write('Y points:\n {}\n\n'.format(y_pts.reshape(len(y_pts), 1)))
        file.write('Temperatures in K\n')
        for i in range(len(T)):
            file.write(str(T[i, 0])+'\n')


def mesh(n_x, n_y, x_pts, y_pts):
    x_c = np.zeros(n_x)
    y_c = np.zeros(n_y)
    A_h = np.zeros_like(x_c)
    A_v = np.zeros_like(y_c)
    dx = np.zeros([n_x - 1])
    dy = np.zeros([n_y - 1])

    for i in range(n_x):
        x_c[i] = (x_pts[i] + x_pts[i + 1]) / 2
        A_h[i] = x_pts[i + 1] - x_pts[i]
    for i in range(n_x - 1):
        dx[i] = (x_pts[i + 1] + x_pts[i + 2]) / 2 - x_c[i]
    for i in range(n_y):
        y_c[i] = (y_pts[i] + y_pts[i + 1]) / 2
        A_v[i] = y_pts[i + 1] - y_pts[i]
    for i in range(n_y - 1):
        dy[i] = (y_pts[i + 1] + y_pts[i + 2]) / 2 - y_c[i]

    return x_c, y_c, A_h, A_v, dx, dy


def discretization(l, n_x, n_y, th, discr):
    if discr == 'unif':
        x_pts = np.linspace(0, l, n_x+1)
        y_pts = np.linspace(0, th, n_y+1)

        return x_pts, y_pts

    if discr == 'cos':
        x_pts = (1 - np.cos(np.linspace(0, np.pi/2, n_x+1))) * l
        y_pts = np.linspace(0, th, n_y + 1)

        return x_pts, y_pts

    if discr == 'tan':
        x_pts = (np.tan(np.linspace(0, np.pi/4, n_x + 1))) * l
        y_pts = np.linspace(0, th, n_y + 1)

        return x_pts, y_pts


def cds(nx, ny, k, a_v, a_h, dx, dy, x_c, y_c, h, h_r, T_b, T_inf, T_s):
    # Preallocating arrays
    A_e = np.zeros([ny, nx])
    A_w = np.zeros_like(A_e)
    A_n = np.zeros_like(A_e)
    A_s = np.zeros_like(A_e)
    S_p = np.zeros_like(A_e)

    dx = dx.reshape([dx.shape[0], 1])
    dy = dy.reshape([dy.shape[0], 1])
    a_v = a_v.reshape([1, a_v.shape[0]])
    a_h = a_h.reshape([1, a_h.shape[0]])
    
    A_e[:, :-1] = ((k / dx) @ a_v).T
    A_w[:, 1:] = ((k / dx) @ a_v).T
    A_n[1:, :] = ((k / dy) @ a_h)
    A_s[:-1, :] = ((k / dy) @ a_h)
    A_p = -(A_e + A_w + A_n + A_s)

    # Boundary condition

    # Surface 1 Top
    A_p[0, :] = A_p[0, :] - a_h * (h + h_r[0, :])
    S_p[0, :] = S_p[0, :] - a_h * (h * T_inf + h_r[0, :] * T_s)

    # Surface 2 Tip

    # Surface 3 Bottom
    A_p[-1, :] = A_p[-1, :] - a_h * (h + h_r[-1, :])
    S_p[-1, :] = S_p[-1, :] - a_h * (h * T_inf + h_r[-1, :] * T_s)

    # Surface 0 Base (Neumann)
    A_p[:, 0] = A_p[:, 0] - k / x_c[0] * a_v
    S_p[:, 0] = S_p[:, 0] - (k / x_c[0]) * a_v * T_b

    # Diagonals reshape (linear indexing)
    A_e = np.roll(A_e.reshape([nx * ny, 1])[:, 0], 1)
    A_w = np.roll(A_w.reshape([nx * ny, 1])[:, 0], -1)
    A_n = np.roll(A_n.reshape([nx * ny, 1])[:, 0], -nx)
    A_s = np.roll(A_s.reshape([nx * ny, 1])[:, 0], nx)
    A_p = A_p.reshape([nx * ny, 1])[:, 0]
    S_p = S_p.reshape([nx * ny, 1])[:, 0]

    A = spdiags([A_n, A_w, A_p, A_e, A_s], [-nx, -1, 0, 1, nx], m=nx*ny, n=nx*ny, format='csr')
    return A, S_p


def compute(inp_path=None, convection=False, discr='unif', debug=False, n_x=None, n_y=None):
    start(80)

    if inp_path is None:
        inp_path = input('Full path to the input file: ')

    data = read_input(inp_pat=inp_path)

    # Input variables and costants
    T_b = data.Value.Plate_temperature
    T_inf = data.Value.Air_temperature
    T_s = data.Value.Surrounding_temperature
    l = data.Value.Fin_length
    h = data.Value.Heat_transfer_coefficient
    e = data.Value.Emissivity
    k = data.Value.Thermal_conductivity
    th = data.Value.Thickness
    nx = int(data.Value.x_discretization)
    ny = int(data.Value.y_discretization)
    res = data.Value.Residual

    sb = 5.670367e-8  # Stefan-Boltzman costant

    # Transforming temperatures in kelvin
    T_b = c2k(T_b)
    T_inf = c2k(T_inf)
    T_s = c2k(T_s)

    if debug:
        nx = n_x
        ny = n_y

    # Creating the mesh
    x_pts, y_pts = discretization(l=l, n_x=nx, n_y=ny, th=th, discr=discr)
    x_c, y_c, a_h, a_v, dx, dy = mesh(n_x=nx, n_y=ny, x_pts=x_pts, y_pts=y_pts)

    # Values initialization
    h_r = np.zeros([ny, nx])
    T = np.zeros([nx*ny, 1])

    # Iteration loop
    it = 0
    while 1:
        it += 1

        if convection:
            if it == 1:
                h_r = np.zeros([ny, nx])  # First iteration with convection turned off
            else:
                h_r = (e * sb * (T ** 2 + T_s ** 2) * (T + T_s)).reshape([ny, nx])

        A, S = cds(nx=nx, ny=ny, k=k, a_v=a_v, a_h=a_h, dx=dx, dy=dy, x_c=x_c, y_c=y_c, h=h, h_r=h_r, T_b=T_b,
                    T_inf=T_inf, T_s=T_s)
        
        T_new = spsolve(A, S).reshape([nx*ny, 1])
        diff = T - T_new

        T = T_new.copy()

        if abs(diff).max() < res:
            break
        if it == 20:
            print('Error: diff ={:}'.format(abs(diff).max()))
            break

    q = (- k * a_v * (T_new[np.arange(0, nx*ny, nx)] - T_b)[:, 0] / x_c[0]).sum()
    output(q=q, T=T, file_out='out.dat', x_pts=x_pts, y_pts=y_pts)
    end(out_len=80, i=it, q=q, nx=nx, ny=ny)
    return q


if __name__ == '__main__':
    input_file = r'..\Test\Input_test.dat'

    prova = compute(input_file, convection=True, discr='tan')
